package com.matt.smaster.test;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MasterTester {
	
	private static String address = "127.0.0.1";
	private static int port = 24000;

	private static Logger logger;
	
	private static MasterTester instance;
	
	public static MasterTester getInstance()
	{
		if (instance == null)
			instance = new MasterTester();
		
		return instance;
	}
	
	public static final void main(String[] args)
	{
		logger = Logger.getLogger(MasterTester.class.getName());
		try {
			MasterTester.getInstance().start();
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Failed to start tester", ex);
		}
	}
	
	private Socket socket;
	
	private MasterTester()
	{
		
	}
	
	public void start() throws UnknownHostException, IOException
	{
		System.out.println("Connecting to server " + address + ":" + port + " ...");
		socket = new Socket(address, port);
		System.out.println("Connected");
		
		System.out.println("Disconnecting...");
		socket.close();
		System.out.println("Disconnected");
	}
	
}
