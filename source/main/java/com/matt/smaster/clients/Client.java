package com.matt.smaster.clients;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.matt.smaster.MasterServer;
import com.matt.smaster.events.queue.MessageQueue;
import com.matt.smaster.events.types.ClientConfiguredEvent;
import com.matt.smaster.events.types.ClientConfiguredJoinEvent;
import com.matt.smaster.events.types.ClientLeaveEvent;
import com.matt.smaster.events.types.DataReceivedEvent;
import com.matt.smaster.events.types.UDPDataReceivedEvent;

public class Client {
	
	private Logger logger;
	
	private Socket socket;
	private BufferedReader input;
	private BufferedOutputStream output;
	private MessageQueue queue;
	
	private Thread statusThread;
	private Thread inputThread;
	private Thread outputThread;
	
	private String id;
	private String gameId;
	
	private String extraInfo;
	
	private boolean hostingConfigured;
	private boolean joiningConfigured;
	
	private boolean joinOpen;
	
	private String udpAddress;
	private int udpPort;
	
	private long lastHeartbeatTime;
	
	public Client(Socket socket)
	{
		this.socket = socket;
		this.queue = new MessageQueue();
		this.hostingConfigured = false;
		this.joiningConfigured = false;
		
		this.logger = Logger.getLogger(Client.class.getName());
	}
	
	public void start()
	{
		try {
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new BufferedOutputStream(socket.getOutputStream());
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Failed to create streams", ex);
			this.close();
			return;
		}
		
		statusThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				while(true)
				{
					Client.this.updateStatus();
					if (!Client.this.isActive())
						break;
					
					try {
						Thread.sleep(5000);
					} catch (InterruptedException ex)
					{
						System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
					}
				}
			}
		});
		statusThread.start();
		
		inputThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				while(true)
				{
					try {
						Client.this.updateInput();
					} catch (IOException ex) {
						logger.log(Level.SEVERE, "Failed to process input", ex);
						Client.this.close();
					}
					if (!Client.this.isActive())
						break;
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException ex)
					{
						System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
					}
				}
			}
		});
		inputThread.start();
		
		outputThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				while(true)
				{
					try {
						Client.this.updateOutput();
					} catch (IOException ex) {
						logger.log(Level.SEVERE, "Failed to process input", ex);
						Client.this.close();
					}
					if (!Client.this.isActive())
						break;
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException ex)
					{
						System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
					}
				}
			}
		});
		outputThread.start();
	}
	
	private void updateStatus()
	{
		//System.out.println("Update Status");
		if (this.socket.isClosed() || this.socket.isInputShutdown() || this.socket.isOutputShutdown())
		{
			close();
			return;
		}
		
		if (this.hostingConfigured)
		{
			long newTime = System.currentTimeMillis();
			
			long timeSinceHeartbeat = newTime - lastHeartbeatTime;
			
			if (timeSinceHeartbeat > 20000)
			{
				System.out.println("Client " + this.getId() + " expired");
				this.close();
			}
		}
		
		//this.sendData("KEEP-ALIVE");
	}
	
	private void updateInput() throws IOException
	{
		try {
			String line = input.readLine();
			if (line == null)
			{
				this.close();
				return;
			}
			System.out.println("Input: " + line);
			
			line = line.trim();
			
			MasterServer.getInstance().bus.post(new DataReceivedEvent(this, line));
		} catch (SocketException ex)
		{
			this.close();
		}
	}
	
	private void updateOutput() throws IOException
	{
		try {
			while(this.queue.hasNext())
			{
				byte[] data = this.queue.next();
				
				this.output.write(data);
				this.output.flush();
			}
		} catch (SocketException ex)
		{
			this.close();
		}
	}
	
	public void sendData(byte[] data)
	{
		this.queue.queue(data);
	}
	
	public void sendData(String data)
	{
		System.out.println("Sending Data: " + data);
		this.sendData((data + '\n').getBytes());
	}
	
	public void waitClose()
	{
		while (this.queue.hasNext())
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex)
			{
				System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
			}
		}
		
		this.close();
	}
	
	public void close()
	{
		if (!this.socket.isClosed())
		{
			try {
				this.socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (ClientManager.contains(this))
			MasterServer.getInstance().bus.post(new ClientLeaveEvent(this));
		
		ClientManager.remove(this);
	}
	
	public void heartbeat()
	{
		this.lastHeartbeatTime = System.currentTimeMillis();
	}
	
	public boolean checkOutdated(String clientVersion)
	{
		boolean outdatedClient = false;
		boolean outdatedServer = false;
		try {
			int cVersion = Integer.parseInt(clientVersion);
			if (cVersion < MasterServer.VERSION)
				outdatedClient = true;
			else if (MasterServer.VERSION < cVersion)
				outdatedServer = true;
		} catch (NumberFormatException ex)
		{
			outdatedClient = true;
		}
		if (outdatedClient || outdatedServer)
		{
			this.sendData("outOfDate:" + (outdatedServer ? "true" : "false"));
			this.waitClose();
		}
		return outdatedClient || outdatedServer;
	}
	
	public String getAddress()
	{
		return this.socket.getInetAddress().getHostAddress();
	}
	
	public int getTCPPort()
	{
		return this.socket.getPort();
	}
	
	public boolean isActive()
	{
		return !this.socket.isClosed();
	}
	
	public String getId()
	{
		return this.id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getGameId()
	{
		return this.gameId;
	}
	
	public void setGameId(String gameId)
	{
		this.gameId = gameId;
	}
	
	public boolean isHostingConfigured()
	{
		return this.hostingConfigured;
	}
	
	public void configureHost(String address, int port)
	{
		this.udpAddress = address;
		this.udpPort = port;
		this.hostingConfigured = true;
		this.heartbeat();
		
		System.out.println("Client " + this.id + " configured for hosting");
		
		MasterServer.getInstance().bus.post(new ClientConfiguredEvent(this));
	}
	
	public boolean isJoiningConfigured()
	{
		return this.joiningConfigured;
	}
	
	public void configureJoin(String address, int port)
	{
		this.udpAddress = address;
		this.udpPort = port;
		this.joiningConfigured = true;
		
		System.out.println("Client " + this.id + " configured for joining");
		
		MasterServer.getInstance().bus.post(new ClientConfiguredJoinEvent(this));
	}
	
	public void receiveUDPData(String message)
	{
		MasterServer.getInstance().bus.post(new UDPDataReceivedEvent(this, message));
	}
	
	public String getUDPAddress()
	{
		return this.udpAddress;
	}
	
	public int getUDPPort()
	{
		return this.udpPort;
	}
	
	public boolean isJoinOpen()
	{
		return this.joinOpen;
	}
	
	public void setJoinOpen(boolean flag)
	{
		this.joinOpen = flag;
	}
	
	public String getExtraInfo()
	{
		return this.extraInfo;
	}
	
	public void setExtraInfo(String extraInfo)
	{
		this.extraInfo = extraInfo;
	}
	
}
