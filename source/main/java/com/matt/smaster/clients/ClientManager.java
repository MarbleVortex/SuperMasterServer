package com.matt.smaster.clients;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ClientManager {
	
	private static final List<Client> clients = new ArrayList<Client>();
	
	public static final void add(Client client)
	{
		clients.add(client);
	}
	
	public static final Client get(int index)
	{
		return clients.get(index);
	}
	
	public static final int indexOf(Client client)
	{
		return clients.indexOf(client);
	}
	
	public static final void remove(int index)
	{
		clients.remove(index);
	}
	
	public static final void remove(Client client)
	{
		clients.remove(client);
	}
	
	public static final boolean contains(Client client)
	{
		return clients.contains(client);
	}
	
	public static final int size()
	{
		return clients.size();
	}
	
	public static Iterator<Client> iterator()
	{
		return clients.iterator();
	}
	
	private ClientManager() { }
	
}
