package com.matt.smaster.events;

import com.matt.smaster.events.listeners.MainListener;

public final class EventListeners {
	
	public static final void registerListeners(EventBus bus)
	{
		bus.registerListener(new MainListener());
	}
	
	private EventListeners() { }
	
}
