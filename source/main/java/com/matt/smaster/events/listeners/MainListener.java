package com.matt.smaster.events.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.matt.smaster.clients.Client;
import com.matt.smaster.clients.ClientManager;
import com.matt.smaster.events.Subscribe;
import com.matt.smaster.events.types.ClientJoinEvent;
import com.matt.smaster.events.types.ClientLeaveEvent;
import com.matt.smaster.events.types.DataReceivedEvent;
import com.matt.smaster.events.types.UDPDataReceivedEvent;

public class MainListener {
	
	private Logger logger;
	
	public MainListener()
	{
		this.logger = Logger.getLogger(MainListener.class.getName());
	}
	
	@Subscribe
	private void onClientJoin(ClientJoinEvent e)
	{
		Client client = e.getClient();
		String id = UUID.randomUUID().toString();
		client.setId(id);
		System.out.println("Client " + client.getId() + " Joined From: " + client.getAddress() + ":" + client.getTCPPort());
		
		client.sendData("requestValidation:" + client.getId());
	}
	
	@Subscribe
	private void onClientLeave(ClientLeaveEvent e)
	{
		Client client = e.getClient();
		System.out.println("Client " + client.getId() + " Left");
	}
	
	@Subscribe
	private void onDataReceived(DataReceivedEvent e)
	{
		Client client = e.getClient();
		String msg = e.getMessage();
		
		String responseIdCmd = "responseValidation:";
		if (msg.startsWith(responseIdCmd))
		{
			String[] data = msg.substring(responseIdCmd.length()).split(":");
			if (data.length != 4)
			{
				logger.log(Level.WARNING, "Invalid Info Response Received");
				client.close();
				return;
			}
			
			String versionString = data[0].trim();
			String gameId = data[1].trim();
			boolean joinOpen = data[2].trim().equals("true");
			String extraInfo = data[3].trim();

			if (client.checkOutdated(versionString))
				return;
			
			client.setGameId(gameId);
			client.setJoinOpen(joinOpen);
			client.setExtraInfo(extraInfo);
			
			System.out.println("Validated Client " + client.getAddress() + ":" + client.getTCPPort() + " with id " + client.getId() + " - JoinOpen: " + (joinOpen ? "true" : "false"));
			
			client.sendData("requestUDP");
			
		} else {
			String requestQueryCmd = "requestQuery:";
			if (msg.startsWith(requestQueryCmd))
			{
				List<Client> availableHosts = new ArrayList<Client>();
				
				String[] data = msg.substring(requestQueryCmd.length()).trim().split(":");
				if (data.length != 2)
				{
					logger.log(Level.WARNING, "Invalid Query Request Received");
					client.close();
					return;
				}
				
				String versionString = data[0];
				String gameId = data[1].trim();

				if (client.checkOutdated(versionString))
					return;
				
				for (int i = 0; i < ClientManager.size(); i++)
				{
					Client c = ClientManager.get(i);
					
					if (c != client && c.isHostingConfigured() && c.isActive() && c.isJoinOpen() && c.getGameId().equals(gameId))
					{
						availableHosts.add(c);
					}
				}
				
				StringBuilder queryResponse = new StringBuilder("responseQuery:");
				
				if (availableHosts.size() > 0)
				{
					for (int i = 0; i < availableHosts.size(); i++)
					{
						Client c = availableHosts.get(i);
						
						queryResponse.append(c.getId() + ":" + c.getUDPAddress() + ":" + c.getUDPPort() + ":" + c.getGameId() + ":" + c.getExtraInfo() + "|");
					}
				} else {
					queryResponse.append("none");
				}
				
				client.sendData(queryResponse.toString());
				
				System.out.println("Responded to query request");
			} else {
				if (msg.equals("responseUDP"))
				{
					boolean success = true;
					if (!client.isHostingConfigured())
					{
						System.err.println("Failed to broadcast server. Never received Id over UDP.");
						success = false;
					}
					client.sendData("responseBroadcast:" + (success ? "true" : "false"));
					
					if (!success)
						client.waitClose();
					else
						client.heartbeat();
				} else {
					String requestJoinCmd = "requestJoin:";
					if (msg.startsWith(requestJoinCmd))
					{
						String[] data = msg.substring(requestJoinCmd.length()).trim().split(":");
						if (data.length != 2)
						{
							logger.log(Level.WARNING, "Invalid Join Request Received");
							client.close();
							return;
						}
						
						String versionString = data[0];
						String joinId = data[1];

						if (client.checkOutdated(versionString))
							return;
						
						System.out.println("Requesting Join UDP For Client " + client.getAddress() + ":" + client.getTCPPort() + " with id " + client.getId());
						
						client.sendData("requestJoinUDP:" + joinId);
					} else {
						String responseJoinUDPCmd = "responseJoinUDP:";
						if (msg.startsWith(responseJoinUDPCmd))
						{
							String joinId = msg.substring(responseJoinUDPCmd.length());
							System.out.println("Join ID: " + joinId);
							
							Client joinClient = null;
							for (int i = 0; i < ClientManager.size(); i++)
							{
								Client c = ClientManager.get(i);
								if (c.getId().equals(joinId))
								{
									joinClient = c;
									break;
								}
							}
							
							boolean success = true;
							
							if (joinClient == null)
							{
								success = false;
							}
							
							String joinAddress = null;
							int joinPort = 0;
							
							if (joinClient != null)
							{
								joinAddress = joinClient.getUDPAddress();
								joinPort = joinClient.getUDPPort();
							}
							
							if (!client.isJoiningConfigured())
							{
								System.err.println("Failed to join server. Never received Id over UDP.");
								success = false;
							}
							client.sendData("responseJoin:" + (success ? "true" : "false") + ":" + joinAddress + ":" + joinPort);
							
							if (success)
							{
								joinClient.sendData("requestClientJoin:" + client.getUDPAddress() + ":" + client.getUDPPort());
							} else {
								client.waitClose();
							}
						} else
						{
							String heartbeatCmd = "HEARTBEAT";
							if (msg.equals(heartbeatCmd))
							{
								client.heartbeat();
							}
						}
					}
				}
			}
		}
	}
	
	@Subscribe
	public void onReceiveUDPData(UDPDataReceivedEvent event)
	{
		Client client = event.getClient();
		System.out.println("Received: \"" + event.getMessage() + "\" from " + client.getAddress() + ":" + client.getUDPPort() + " over UDP");
	}
	
}
