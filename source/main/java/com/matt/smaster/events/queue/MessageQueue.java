package com.matt.smaster.events.queue;

import java.util.ArrayList;
import java.util.List;

public class MessageQueue {
	
	private List<byte[]> queue;
	
	public MessageQueue()
	{
		this.queue = new ArrayList<byte[]>();
	}
	
	public synchronized void queue(byte[] data)
	{
		queue.add(data);
	}
	
	public synchronized byte[] next()
	{
		if (!hasNext())
			return null;
		
		byte[] ret = this.queue.get(0);
		this.queue.remove(0);
		return ret;
	}
	
	public synchronized boolean hasNext()
	{
		return !queue.isEmpty();
	}
	
	public synchronized int getRemaining()
	{
		return queue.size();
	}
	
}
