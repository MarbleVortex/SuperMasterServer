package com.matt.smaster.events.types;

import com.matt.smaster.clients.Client;
import com.matt.smaster.events.Event;

public abstract class ClientEvent extends Event {
	
	protected Client client;
	
	public ClientEvent(Client client)
	{
		this.client = client;
	}
	
	public Client getClient()
	{
		return client;
	}
	
}
