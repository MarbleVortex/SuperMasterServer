package com.matt.smaster.events.types;

import com.matt.smaster.clients.Client;

public class DataReceivedEvent extends ClientEvent {
	
	private String message;
	
	public DataReceivedEvent(Client client, String message)
	{
		super(client);
		this.message = message;
	}
	
	public String getMessage()
	{
		return this.message;
	}
	
}
