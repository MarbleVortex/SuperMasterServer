package com.matt.smaster.events.types;

import com.matt.smaster.clients.Client;

public class UDPDataReceivedEvent extends ClientEvent {
	
	private String message;
	
	public UDPDataReceivedEvent(Client client, String message)
	{
		super(client);
		this.message = message;
	}
	
	public String getMessage()
	{
		return this.message;
	}
	
}
