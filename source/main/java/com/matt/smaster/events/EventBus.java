package com.matt.smaster.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventBus {
	
	private Logger logger;
	
	private List<Object> listeners;
	
	public EventBus()
	{
		this.logger = Logger.getLogger(EventBus.class.getName());
		this.listeners = new ArrayList<Object>();
	}
	
	public void registerListener(Object obj)
	{
		this.listeners.add(obj);
	}
	
	public void removeListener(Object obj)
	{
		if (this.listeners.contains(obj))
			this.listeners.remove(obj);
	}
	
	public boolean containsListener(Object obj)
	{
		return this.listeners.contains(obj);
	}
	
	public void post(Event event)
	{
		Thread postThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				for (Object obj : listeners)
				{
					Class<?> listenerClass = obj.getClass();
					
					Method methods[] = listenerClass.getDeclaredMethods();
					
					for (Method m : methods)
					{
						Subscribe sub = m.getAnnotation(Subscribe.class);
						if (sub != null)
						{
							Class<?>[] params = m.getParameterTypes();
							if (params.length == 1 && params[0].equals(event.getClass()))
							{
								try {
									m.setAccessible(true);
									m.invoke(obj, event);
								} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
									logger.log(Level.WARNING, "Failed to invoke event", ex);
								}
							}
						}
					}
				}
			}
		});
		postThread.start();
	}
}
