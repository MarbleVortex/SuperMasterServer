package com.matt.smaster;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.matt.smaster.clients.Client;
import com.matt.smaster.clients.ClientManager;
import com.matt.smaster.events.EventBus;
import com.matt.smaster.events.EventListeners;
import com.matt.smaster.events.types.ClientJoinEvent;

public class MasterServer {
	
	public static final int VERSION = 1;
	
	private static int tcpPort = 24000;
	private static int udpPort = 24001;
	
	private static Logger logger;
	
	private static MasterServer instance;
	
	public static final MasterServer getInstance()
	{
		if (instance == null)
			instance = new MasterServer();
		
		return instance;
	}
	
	public static final void main(String[] args)
	{
		logger = Logger.getLogger(MasterServer.class.getName());
		try {
			getInstance().start();
		} catch (IOException ex)
		{
			logger.log(Level.SEVERE, "Failed to start server", ex);
		}
	}
	
	private ServerSocket server;
	private Thread connectionThread;
	private DatagramSocket udpSocket;
	private Thread udpThread;
	private boolean running;
	
	public final EventBus bus;
	
	public MasterServer()
	{
		this.bus = new EventBus();
		EventListeners.registerListeners(this.bus);
	}
	
	public void start() throws IOException
	{
		if (running)
			return;
		running = true;
		
		server = new ServerSocket(tcpPort);
		udpSocket = new DatagramSocket(udpPort);
		System.out.println("Started server on port " + server.getLocalPort() + " and " + udpSocket.getLocalPort());
		
		startThreads();
	}
	
	public void startThreads()
	{
		udpThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				while(MasterServer.this.running)
				{
					try {
						MasterServer.this.updateUDP();
					} catch (IOException ex)
					{
						logger.log(Level.SEVERE, "Failed to accept client", ex);
					}
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException ex)
					{
						System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
					}
				}
			}
		});
		
		connectionThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				while(MasterServer.this.running)
				{
					try {
						MasterServer.this.runConnections();
					} catch (IOException ex) {
						logger.log(Level.SEVERE, "Failed to accept client", ex);
					}
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException ex)
					{
						System.err.println("Error during thread sleep: " + ex.getLocalizedMessage());
					}
				}
			}
		});
		
		udpThread.start();
		connectionThread.start();
		
		System.out.println("Initialization Complete");
		System.out.println("Waiting for connections...");
	}
	
	public void updateUDP() throws IOException
	{
		//System.out.println("UDP STUFF");
		DatagramPacket receivePacket = new DatagramPacket(new byte[2048], 2048);
		udpSocket.receive(receivePacket);
		
		String address = receivePacket.getAddress().getHostAddress();
		int port = receivePacket.getPort();
		
		String message = new String(receivePacket.getData()).trim();
		
		String idCmd = "setId:";
		if (message.startsWith(idCmd))
		{
			String[] data = message.substring(idCmd.length()).split(":");
			if (data.length != 2)
			{
				logger.log(Level.WARNING, "Invalid set id request received");
				return;
			}
			
			String versionString = data[0];
			String id = data[1];
			
			Client client = null;
			for (int i = 0; i < ClientManager.size(); i++)
			{
				Client c = ClientManager.get(i);
				
				if (!c.isHostingConfigured() && !c.isJoiningConfigured() && id.equalsIgnoreCase(c.getId()))
				{
					client = c;
					break;
				}
			}
			
			if (client == null)
				return;

			if (client.checkOutdated(versionString))
				return;
				
			client.configureHost(address, port);
		} else {
			String idJoinCmd = "setJoinId:";
			if (message.startsWith(idJoinCmd))
			{
				String[] data = message.substring(idJoinCmd.length()).split(":");
				if (data.length != 2)
				{
					logger.log(Level.WARNING, "Invalid set join id request received");
					return;
				}

				String versionString = data[0];
				String id = data[1];
				
				Client client = null;
				for (int i = 0; i < ClientManager.size(); i++)
				{
					Client c = ClientManager.get(i);
					
					if (!c.isJoiningConfigured() && !c.isHostingConfigured() && id.equalsIgnoreCase(c.getId()))
					{
						client = c;
						break;
					}
				}
				
				if (client == null)
					return;

				if (client.checkOutdated(versionString))
					return;
					
				client.configureJoin(address, port);
			} else {
				Client client = null;
				for (int i = 0; i < ClientManager.size(); i++)
				{
					Client c = ClientManager.get(i);
					
					if (c.isHostingConfigured() && address.equals(c.getUDPAddress()) && port == c.getUDPPort())
					{
						client = c;
					}
				}
				
				if (client == null)
					return;
				
				client.receiveUDPData(message);
			}
		}
	}
	
	public void runConnections() throws IOException
	{
		//System.out.println("CON STUFF");
		Socket socket = this.server.accept();
		//System.out.println("CON: " + socket.getInetAddress().getHostAddress() + ":" + socket.getPort());
		Client client = new Client(socket);
		ClientManager.add(client);
		client.start();
		
		this.bus.post(new ClientJoinEvent(client));
	}
	
	public void disconnectAll()
	{
		for (int i = 0; i < ClientManager.size(); i++)
		{
			Client client = ClientManager.get(i);
			client.close();
		}
	}
	
	public void stop()
	{
		if (!running)
			return;
		running = false;
		
		disconnectAll();
		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		udpSocket.close();
	}
	
}
